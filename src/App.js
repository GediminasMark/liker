import React, {Component} from 'react';
import './App.css';
import SuggestionsTable from "./containers/SuggestionsTable";
import SuggestionsForm from "./containers/SuggestionForm";

class Liker extends Component {
    render() {
        return (
            <div className='liker-container'>
                <SuggestionsTable/>
                <SuggestionsForm/>
            </div>
        );
    }
}

export default Liker;
