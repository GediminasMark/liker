import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from "classnames";

export class Input extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }
    onChange(e) {
        let value = e.target.value;
        this.props.onChange(value);
    }

    render() {
        let {className} = this.props;
        return (<input className={classNames(className)} type="text" placeholder={this.props.placeholder} value={this.props.value}
                       onChange={this.onChange}/>)
    }
}

Input.propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func,
    placeholder: PropTypes.string
};