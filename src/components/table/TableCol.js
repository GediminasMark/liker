import React from 'react';
import PropTypes from "prop-types";
import classNames from "classnames";

export const TableCol = ({children, className}) => {
    return (<td className={classNames(className)}>{children}</td>);
};

TableCol.propTypes = {
    className: PropTypes.string
};