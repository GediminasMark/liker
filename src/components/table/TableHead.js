import React from 'react';
import PropTypes from "prop-types";
import classNames from "classnames/bind";

export const TableHead = ({children, className}) => {
    return (<thead className={classNames(className)}>{children}</thead>)
};

TableHead.propTypes = {
    className: PropTypes.string
};