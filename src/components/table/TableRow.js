import React from 'react';
import PropTypes from "prop-types";
import classNames from 'classnames';

export const TableRow = ({children, className}) => {
    return (<tr className={classNames('table-row', className)}>{children}</tr>);
};

TableRow.propTypes = {
    className: PropTypes.string
};