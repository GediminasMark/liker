import React from 'react';
import PropTypes from 'prop-types';
import './table.css';
import classNames from "classnames";

export const Table = ({children, className}) => {
    return (
        <table className={classNames('table', className)}>
            {children}
        </table>
    )
};

Table.propTypes = {
    className: PropTypes.string
};

