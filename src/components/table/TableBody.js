import React from 'react';
import PropTypes from "prop-types";
import classNames from "classnames";

export const TableBody = ({children, className}) => {
    return (<tbody className={classNames(className)}>{children}</tbody>)
};

TableBody.propTypes = {
    className: PropTypes.string
};