import React from 'react';
import PropTypes from 'prop-types';
import './button.css';
import classNames from "classnames";

export const Button = ({text, onClick, className}) => {
    return <button
        onClick={onClick}
        className={classNames('btn', className)}>{`${text}`}</button>
};

Button.propTypes = {
    text: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func
}