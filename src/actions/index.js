export const SUGGESTION_ADD = 'SUGGESTION_ADD';
export const SUGGESTION_LIKE = 'SUGGESTION_LIKE';
export const SUGGESTION_UNLIKE = 'SUGGESTION_UNLIKE';

export const addSuggestion = (name) => ({
    type: SUGGESTION_ADD,
    name
});

export const likeSuggestion = (id) => ({
    type: SUGGESTION_LIKE,
    id
});

export const unlikeSuggestion = (id) => ({
    type: SUGGESTION_UNLIKE,
    id
});

