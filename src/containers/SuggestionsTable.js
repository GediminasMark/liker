import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Table} from "../components/table/Table";
import {TableHead} from "../components/table/TableHead";
import {TableRow} from "../components/table/TableRow";
import {TableCol} from "../components/table/TableCol";
import {TableBody} from "../components/table/TableBody";
import {likeSuggestion, unlikeSuggestion} from "../actions";
import {connect} from "react-redux";
import {Button} from "../components/button/Button";
import './SuggestionsTable.css';
import classNames from 'classnames';

const SuggestionsTableItem = ({suggestion, onClick}) => {
    return (<TableRow>
        <TableCol className='suggestions-table__body__name'>{`${suggestion.name}`}</TableCol>
        <TableCol className='suggestions-table__body__action'>
            <Button text={`${suggestion.likes}`}
                    className={classNames('suggestion-like__button', {'suggestion-like__button--liked': (suggestion.likedByMe)})}
                    onClick={() => onClick(suggestion)}/>
        </TableCol>
    </TableRow>)
};

SuggestionsTableItem.propType = {
    suggestion: PropTypes.object.required,
    onClick: PropTypes.func
};

class SuggestionsTableContainer extends Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick(suggestion) {
        (suggestion.likedByMe) ? this.props.unlikeSuggestion(suggestion.id) : this.props.likeSuggestion(suggestion.id);
    }

    render() {
        let {suggestions} = this.props;
        let SuggestionsBody = suggestions.map((suggestion) => <SuggestionsTableItem key={suggestion.id}
                                                                                    suggestion={suggestion}
                                                                                    onClick={this.onClick}/>);
        return (<Table className='suggestions-table'>
            <TableHead className='suggestions-table__head'>
                <TableRow>
                    <TableCol className='suggestions-table__head__name'>Suggestion</TableCol>
                    <TableCol className='suggestions-table__head__action'>Likes</TableCol>
                </TableRow>
            </TableHead>
            <TableBody className='suggestions-table__body'>
                {SuggestionsBody}
            </TableBody>
        </Table>)
    }

}

const mapStateToProps = (state) => ({
    suggestions: state.suggestions
});

const mapDispatchToProps = (dispatch) => ({
    likeSuggestion: (id) => dispatch(likeSuggestion(id)),
    unlikeSuggestion: (id) => dispatch(unlikeSuggestion(id)),
});

const SuggestionsTable = connect(
    mapStateToProps,
    mapDispatchToProps
)(SuggestionsTableContainer);

export default SuggestionsTable;