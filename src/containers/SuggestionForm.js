import React, {Component} from 'react';
import {Table} from "../components/table/Table";
import {TableRow} from "../components/table/TableRow";
import {TableCol} from "../components/table/TableCol";
import {TableBody} from "../components/table/TableBody";
import {addSuggestion} from "../actions";
import {connect} from "react-redux";
import {Button} from "../components/button/Button";
import {Input} from "../components/input/Input";
import './SuggestionForm.css';

class SuggestionFormContainer extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.state = {
            value: ""
        }
    }

    onChange(value) {
        this.setState({
            value
        });
    }

    onSubmit() {
        if (this.state.value !== "") {
            this.props.addSuggestion(this.state.value);
            this.setState({value: ""});
        }
    }

    render() {
        return (
            <Table className='suggestions-form'>
                <TableBody>
                    <TableRow>
                        <TableCol>
                            <Input placeholder={`Add new suggestion`} className='suggestion-name__input' onChange={this.onChange}
                                   value={this.state.value}/>
                        </TableCol>
                        <TableCol className='suggestions-form__actions'>
                            <Button text={`Add`} className='suggestion-add__button'
                                    onClick={this.onSubmit}/>
                        </TableCol>
                    </TableRow>
                </TableBody>
            </Table>
        )
    }

};

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
    addSuggestion: (text) => dispatch(addSuggestion(text)),
});

const SuggestionsForm = connect(
    mapStateToProps,
    mapDispatchToProps
)(SuggestionFormContainer);

export default SuggestionsForm;