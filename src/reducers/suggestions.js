import {
    SUGGESTION_ADD,
    SUGGESTION_LIKE,
    SUGGESTION_UNLIKE
} from './../actions';

const initialState = [
    {
        id: 0,
        name: 'Item 1',
        likes: 1,
        likedByMe: false,
    },
    {
        id: 1,
        name: 'Item 2',
        likes: 5,
        likedByMe: true,
    },
    {
        id: 2,
        name: 'Item 3',
        likes: 1,
        likedByMe: false,
    },
    {
        id: 3,
        name: 'Item 4',
        likes: 0,
        likedByMe: false,
    }
];

export default function (state = initialState, action) {
    switch (action.type) {
        case SUGGESTION_ADD:
            return [
                ...state,
                {
                    id: state.reduce((maxId, suggestion) => Math.max(suggestion.id, maxId), -1) + 1,
                    name: action.name,
                    likes: 0,
                    likedByMe: false
                }
            ];
        case SUGGESTION_LIKE:
            return state.map(suggestion => suggestion.id === action.id ? {
                ...suggestion,
                likes: suggestion.likes + 1,
                likedByMe: true,
            } : suggestion);
        case SUGGESTION_UNLIKE:
            return state.map(suggestion => suggestion.id === action.id ? {
                ...suggestion,
                likes: (suggestion.likes === 0) ? 0 : suggestion.likes - 1,
                likedByMe: false
            } : suggestion);
        default:
            return state;
    }
}